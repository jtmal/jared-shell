#!/bin/bash

# Minecraft RCON Script
# Written by: Jesse T. Maloney
#
# Below you will see the output of "mcrcon" without
# any arguments to see your options in executing the
# command.


# Usage: mcrcon [OPTIONS]... [COMMANDS]...
# Sends rcon commands to minecraft server.
#
# Option:
#   -h		Prints usage.
#   -s		Silent mode. Do not print data received from rcon.
#   -t		Terminal mode. Acts as interactive terminal.
#   -p		Rcon password. Default: "".
#   -H		Host address or ip.
#   -P		Port. Default: 25575.
#   -c		Do not print colors. Disables bukkit color printing.
#
# Invidual commands must be separated with spaces.
#
# Example:
# mcrcon -c -H 192.168.1.42 -P 9999 -p password cmd1 "cmd2 with spaces"
#
# minecraft rcon (mcrcon) 0.0.5.
# Report bugs to tiiffi_at_gmail_dot_com.

CMDS=$@
HOST="162.217.177.249"
PASSWORD=""
ARGSTRING="-H $HOST -p $PASSWORD"
/usr/bin/mcrcon $ARGSTRING "$CMDS"

