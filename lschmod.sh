#!/bin/bash

# Directory Listing Script
# Written by: Jesse T. Maloney
#
# This script generates a directory listing output that outputs
# a long format 'ls' that converts the chmod variables to be
# numerically legible.

if [ $@ ]
 then
  PWD=$@
 else
  PWD=$(pwd)
fi

echo "Generating Long Format Directory Listing (ls -lah)"
echo "With Numerical CHMOD output for: $PWD" 
ls -lah $PWD | awk '{k=0;for(i=0;i<=8;i++)k+=((substr($1,i+2,1)~/[rwx]/) \
             *2^(8-i));if(k)printf("%0o ",k);print}'

