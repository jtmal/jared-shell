#!/bin/bash

# echo "Waiting for world saving to be disabled"
# while !tail -n1 mc.log | grep "world saving"; do echo -n "."; done

# Setting Variables
# All variables are necessary for script execution, so that
# in the event of server migration, the entire script does
# not need to be rewritten.

SCRIPTNAME='Minecraft World Backup Script'
SCRIPTVER='0.3'
CREATOR='Jesse T. Maloney'
EMAIL='jesse@jtmal.net'

FULLDATE=$(date +'%A, %B %d, %Y')
DATE=$(date +%Y%m%d)

TMUXMCPANE=mc
MCDIR=/home/mc/server
WORLD=gaygeeks
MCMODS=mods
MCCONFIG=config
IDFIX=idfix.txt
BACKUPLOC=/home/jtmal/mcbackup
ARCHIVENAME=$WORLD-$DATE.tar.gz
BKUPLOCUSR=jtmal

OLDPWD=$(pwd)

echo "
$SCRIPTNAME
Version: $SCRIPTVER
Created by: $CREATOR
Contact: $EMAIL
 
Minecraft Directory: $MCDIR
World Directory: $MCDIR/$WORLD
Minecraft Backup Location: $BACKUPLOC
Today's Date: $FULLDATE
Archive name: $ARCHIVENAME
Archive log file: $BACKUPLOC/$WORLD-$DATE.log
 
Starting Minecraft World backup process...." | tee -a $BACKUPLOC/$WORLD-$DATE.log

# Send commands through tmux to save the world and to turn off world saving.
# Turning off world saving allows us to back up the files so that no new
# changes are saved, causing possible corruption.

tmux send-keys -t "mc" '/save-all' Enter
tmux send-keys -t "mc" '/say Turning off world saving to back up the world....' Enter
tmux send-keys -t "mc" '/save-off' Enter

echo "Minecraft World is backing up.....
Please wait....."  | tee -a $BACKUPLOC/$WORLD-$DATE.log

# These commands here begin the archiving process. The script "sleeps"
# for 30 seconds to give Minecraft enough time to successfully turn
# off world saving so that no new changes get written, causing
# the archiving to halt.

cd $MCDIR
sleep 30s
tar -czvf $BACKUPLOC/$ARCHIVENAME $WORLD/ >> $BACKUPLOC/$WORLD-$DATE.log
rm -Rf $BACKUPLOC/$WORLD-CURRENT.tar.gz
cp $BACKUPLOC/$ARCHIVENAME $BACKUPLOC/$WORLD-CURRENT.tar.gz
chown $BKUPLOCUSR. $BACKUPLOC/$ARCHIVENAME $BACKUPLOC/$WORLD-$DATE.log $BACKUPLOC/$WORLD-CURRENT.tar.gz
cd $OLDPWD

# Send commands through tmux to turn world saving back on after
# successful archiving.

tmux send-keys -t "mc" '/say Backup of world complete! Happy building!' Enter
tmux send-keys -t "mc" '/save-on' Enter

echo "Minecraft World backup is complete!
Happy Building!" | tee -a $BACKUPLOC/$WORLD-$DATE.log
